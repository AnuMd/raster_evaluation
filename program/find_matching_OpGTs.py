from natsort import natsorted
import os,re, numpy as np,sys,cv2

from iterative_functions import iterative_functions_class
from find_missing import find_missing_class


class op_find_GT_functions:
    def __init__(self):
        self.iterative_functions_obj = iterative_functions_class()
        self.find_missing_obj = find_missing_class()

    def get_op_evaluation_results(self,op_path,output_img_path):
        op_image_path = op_path+'images/'
        op_sys_text_path = op_path+'text/'
        gt_matched_sys_text_path = op_path + 'gt_matched_sys_text/'


        for text_file in natsorted(os.listdir(op_sys_text_path)):
            text_file_name = str(text_file)
            if '_8_openPlan_details.txt' in text_file_name:
                system_image_name = text_file_name[:-23]
                print system_image_name
                op_image_list = []
                height, width = 0,0
                # --find matching OP image list
                for op_image in natsorted(os.listdir(op_image_path)):
                    op_image_name = str(op_image)
                    if op_image_name[:-6] == system_image_name:
                        cont_list = self.iterative_functions_obj.find_contours(op_image_path+op_image_name)
                        op_contour = cont_list[-2][0]
                        op_image_list.append([op_image_name, op_contour])
                        height, width = self.iterative_functions_obj.get_image_height_width(op_image_path+op_image_name)


                #--if has @least one matching GT
                if len(op_image_list)>0:
                    room_details_file = open(op_sys_text_path + text_file_name, 'r')
                    for op_number, text_line in enumerate(room_details_file):
                        contour_seperate = text_line.split(':')
                        if len(contour_seperate) > 3:
                            # print contour_seperate[0]
                            op_border_points = contour_seperate[2]
                            # ---extract op contour
                            contour_details = re.findall(r"[\w']+", op_border_points)
                            int_contour_details = [int(x) for x in contour_details]
                            chunks = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                            op_sys_contour = self.iterative_functions_obj.convert_points_to_contour(chunks)

                            sys_img = ~(np.zeros((height, width, 3), np.uint8))
                            cv2.drawContours(sys_img, [op_sys_contour], -1, (0, 0, 0), -1)
                            cv2.imwrite(output_img_path + system_image_name + str(op_number) + '-sys.png', sys_img)

                            gt_image_name = self.find_matching_GT_image_contour(op_sys_contour,op_image_list,height, width,output_img_path)

                            if gt_image_name != '':
                                gt_image_name_text = gt_image_name[:-4]
                                gt_image_text_file = open(gt_matched_sys_text_path+gt_image_name_text+'_8_openPlan_details.txt','w')
                                gt_image_text_file.write(text_line)


    def find_matching_GT_image_contour(self,op_sys_contour,op_image_list,height, width,output_img_path):
        gt_image_n = ''
        sys_img = ~(np.zeros((height, width, 3), np.uint8))
        cv2.drawContours(sys_img,[op_sys_contour],-1,(0,0,0),-1)

        for gt_row_num,gt_cont_row in enumerate(op_image_list):
            gt_image_name, gt_cont = gt_cont_row
            gt_img = ~(np.zeros((height, width, 3), np.uint8))
            cv2.drawContours(gt_img, [gt_cont], -1, (0, 0, 0), -1)
            # print gt_image_name

            ji,intersection_area = self.find_missing_obj.finding_JI(gt_img,sys_img,height,width,'')
            # print ji

            # cv2.imwrite(output_img_path+gt_image_name[:-5]+str(gt_row_num)+'sys.png',sys_img)
            cv2.imwrite(output_img_path + gt_image_name[:-4]+'-gt.png', gt_img)

            if ji > 0.9100:
                gt_image_n = gt_image_name
                break

        return gt_image_n


