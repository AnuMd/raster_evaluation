import os
from natsort import natsorted

from evaluate_functions import evaluate_functions_class
from iterative_functions import iterative_functions_class
from find_matching_OpGTs import op_find_GT_functions

class op_evaluation_class:

    def __init__(self):
        pass

    def conduct_evaluation(self,file_name,orginal_img_height, orginal_img_width,output_path,op_path):
        self.evaluate_functions_obj = evaluate_functions_class()
        self.iterative_functions_obj = iterative_functions_class()
        # --prepare open plans
        # op_find_GT_functions_obj = op_find_GT_functions()
        # op_find_GT_functions_obj.get_op_evaluation_results(op_path,output_img_path)

        image_score = 0

        path_op_sys_input = op_path+'gt_matched_sys_text/'
        op_sys_file_list = []
        for text_file in natsorted(os.listdir(path_op_sys_input)):
            text_file_name = str(text_file)
            if file_name[:-4]+'-' in text_file_name:
                op_sys_file_list.append(text_file_name)

        if len(op_sys_file_list)>0:
            for op_sys_file in op_sys_file_list:
                path_gravvitas = path_op_sys_input
                only_image_name = str(op_sys_file)[:-23]

                path_gt = op_path+'OP_GT/'+ only_image_name + '/'

                # --create output path
                evaluation_output_path = output_path + only_image_name + '/'
                self.iterative_functions_obj.make_directory(evaluation_output_path, True)

                # only_image_name = str(op_sys_file)[0:-4]
                # --area comparison
                all_op_data = self.evaluate_functions_obj.extract_poplar_room_data(path_gravvitas, only_image_name)
                gt_room_data = self.evaluate_functions_obj.extract_gt_room_data(path_gt)
                op_area_list = self.evaluate_functions_obj.find_gt_for_rooms(all_op_data, gt_room_data)
                # --JI calculation
                average_op_JI = self.evaluate_functions_obj.compare_poplar_and_gt_JI(orginal_img_height, orginal_img_width,op_area_list, evaluation_output_path)
                # --core region index calculation
                average_op_CR = self.evaluate_functions_obj.compare_poplar_and_gt_CR(orginal_img_height, orginal_img_width, op_area_list, evaluation_output_path)
                # #--line comparison
                # op_data = self.evaluate_functions_obj.extract_poplar_op_data(path_gravvitas, only_image_name, 1500, 2000,evaluation_output_path)
                # gt_line_data = self.evaluate_functions_obj.extract_gt_line_data(path_gt)
                # line_list = self.evaluate_functions_obj.find_gt_for_lines(op_data, gt_line_data)
                # --average pixel ratio calculation
                # average_op_pixel_ratio = self.evaluate_functions_obj.compare_poplar_and_gt_lines(orginal_img_height,orginal_img_width,line_list,evaluation_output_path)

                # print 'average_op_JI',average_op_JI
                # print 'average_op_CR', average_op_CR

                # --assign weight we found
                ji_weight, cr_weight = 0.5, 0.5
                weighted_JI = average_op_JI * ji_weight
                weighted_CR = average_op_CR * cr_weight
                image_score = weighted_JI + weighted_CR
                # print 'IMAGE SCORE', image_score

                # return average_op_JI, average_op_CR, average_op_pixel_ratio, weighted_JI, weighted_CR, image_score

        return image_score


