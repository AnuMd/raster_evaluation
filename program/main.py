import os,json,re,cv2,shutil
from natsort import natsorted
from xlwt import Workbook


from find_missing import find_missing_class
from iterative_functions import iterative_functions_class
from find_matching_OpGTs import op_find_GT_functions
from op_evaluation import op_evaluation_class




class MainFormLogic:
    def __init__(self):
        self.find_missing_obj = find_missing_class()
        image_path = os.getcwd() + '/../input/images/'
        output_img_path = os.getcwd() + '/../output/images/'
        output_excl_path = os.getcwd() + '/../output/excel/'
        sys_json_path = os.getcwd() + '/../input/JSON_SYS/'
        gt_json_path = os.getcwd() + '/../input/JSON_GT/'
        op_path = os.getcwd() + '/../input/Open_plans/'

        if os.path.exists(output_img_path):
            shutil.rmtree(output_img_path)
            os.mkdir(output_img_path)

        #--prepare open plans
        # op_find_GT_functions_obj = op_find_GT_functions()
        # op_find_GT_functions_obj.get_op_evaluation_results(op_path,output_img_path)




        wb = Workbook()

        sheet_name = 'Evaluation results'
        ws = wb.add_sheet(sheet_name)
        # --print header values
        values = ['Image Name','Door TP','Door TP_HC','Door Total HC','Door TP_MC','Door Total MC','Door FP','Total GT Doors','Window TP','Window TP_HC','Win Total HC','Window TP_MC','Win Total MC','Window FP','Total GT Window','Text TP','Text TP_HC','Text Total HC','Text TP_MC','Text Total MC','Text FP','Total GT Text','Text Partial Detect','Stair TP','Stair TP_HC','Stair Total HC','Stair TP_MC','Stair Total MC','Stair FP','Total GT Stair','Wall JI', 'Wall P Inter','Wall R Inter','Wall Sys','Wall GT','SGL text','SGL no text','SGL OP','Total SGL Sys','Total SGL GT','OP text matched','OP no text','OP SGL','Total OP Sys','Total OP GT','Open Plan Score']
        self.add_to_excel_columns(values,ws,0)
        # ws.write(0, 0, 'Image Name')
        # ws.write(0, 1, 'Door TP')
        # ws.write(0, 2, 'Door TP_HC')
        # ws.write(0, 3, 'Door TP_MC')
        # ws.write(0, 4, 'Door FP')
        # ws.write(0, 5, 'Total GT Doors')
        # ws.write(0, 6, 'Door HC_Precision')
        # ws.write(0, 7, 'Door MC_Precision')
        # ws.write(0, 8, 'Door HC_Recall')
        # ws.write(0, 9, 'Door MC_Recall')
        # # ws.write(0, 6, 'Windows TP')
        # # ws.write(0, 7, 'Windows FP')
        # # ws.write(0, 8, 'Total GT Windows')
        # # ws.write(0, 9, 'Window Precision')
        # # ws.write(0, 10, 'Window Recall')
        # # ws.write(0, 11, 'Text TP')
        # # ws.write(0, 12, 'Text FP')
        # # ws.write(0, 13, 'Total GT text')
        # # ws.write(0, 14, 'Text Precision')
        # # ws.write(0, 15, 'Text Recall')
        # # ws.write(0,16,'Text Partial Detect')
        # # ws.write(0, 17, 'Room TP')
        # # ws.write(0, 18, 'Room FP')
        # # ws.write(0, 19, 'Total GT Room')
        # # ws.write(0, 20, 'Room Precision')
        # # ws.write(0, 21, 'Room Recall')
        # # ws.write(0, 22, 'Stairs TP')
        # # ws.write(0, 23, 'Stairs FP')
        # # ws.write(0, 24, 'Total GT Stairs')
        # # ws.write(0, 25, 'Stairs Precision')
        # # ws.write(0, 26, 'Stairs Recall')
        # # ws.write(0, 27, 'Wall JI')
        # ### ws.write(0, 18, 'Open Plan Score')

        row_count = 1
        for file in natsorted(os.listdir(image_path)):
            file_name = str(file)
            print file_name
            # ws.write(row_count, 0, file_name)
            only_image_name = file_name[:-4]
            #--read system JSON
            sys_json_data = self.read_JSON_file(sys_json_path+only_image_name+'.json',system_json=True)
            #--read GT JSON
            gt_json_data = self.read_JSON_file(gt_json_path + only_image_name + '.json',system_json=False)

            #--special: reading GT for Rooms
            #--for cvc dataset
            # gt_sgl_rooms_list, gt_op_rooms_list = self.read_gt_room_JSON_file(gt_json_path + only_image_name + '_rooms.json',cvc=True)
            #--for svg and rsvg dataset
            gt_sgl_rooms_list, gt_op_rooms_list = self.read_gt_room_JSON_file(gt_json_path + only_image_name + '_rooms.json',cvc=False)

            #--- door, window, stair, text calculations
            d_data, w_data, t_data, st_data, wl_data = self.find_missing_obj.find_missing_GT_elements(sys_json_data,gt_json_data,output_img_path+only_image_name,image_path+str(file),unknown=False)
            d_true_positive, d_false_positive, d_gt_total, partial_sys_count, d_HC_count, d_MC_count,d_matched_HC_count,d_matched_MC_count = d_data
            w_true_positive, w_false_positive, w_gt_total, partial_sys_count, w_HC_count, w_MC_count,w_matched_HC_count,w_matched_MC_count = w_data
            t_true_positive, t_false_positive, t_gt_total, t_partial_sys_count, t_HC_count, t_MC_count,t_matched_HC_count,t_matched_MC_count = t_data
            st_true_positive, st_false_positive, st_gt_total, partial_sys_count, st_HC_count, st_MC_count,st_matched_HC_count,st_matched_MC_count = st_data
            gt_jaccard_index_walls, sys_wall_area, gt_wall_area, intersection_area_precision,intersection_area_recall= wl_data

            #-- SGL matches
            sgl_room_text_matched = self.find_missing_obj.find_room_matches(sys_json_data['sgl_rooms_list'],gt_sgl_rooms_list,image_path+str(file),output_img_path+only_image_name,match_room_text=True, iteration=1,sub_area=False)
            sgl_room_polygon_matched = self.find_missing_obj.find_room_matches(sys_json_data['sgl_rooms_list'], gt_sgl_rooms_list,image_path + str(file),output_img_path + only_image_name,match_room_text=False,iteration=2,sub_area=False)
            sgl_subarea_text_matched = self.find_missing_obj.find_room_matches(sys_json_data['op_rooms_list'], gt_sgl_rooms_list,image_path + str(file),output_img_path + only_image_name,match_room_text=True, iteration=3,sub_area=True)

            #-- OP sub area matches
            op_room_text_matched = self.find_missing_obj.find_op_matches(sys_json_data['op_rooms_list'],gt_op_rooms_list,image_path + str(file),output_img_path + only_image_name,match_room_text=True, iteration=1)
            op_room_polygon_matched = self.find_missing_obj.find_op_matches(sys_json_data['op_rooms_list'], gt_op_rooms_list, image_path + str(file),output_img_path + only_image_name,match_room_text=True, iteration=2)
            op_sgl_text_matched = self.find_missing_obj.find_op_matches(sys_json_data['sgl_rooms_list'],gt_op_rooms_list, image_path + str(file),output_img_path + only_image_name,match_room_text=True, iteration=3)
            gt_op_sub_area_count = self.count_gt_op_sub_areas(gt_op_rooms_list)

        # #
        # #     # --compare op plans
        # #     # op_evaluation_obj = op_evaluation_class()
        # #     # iterative_functions_obj = iterative_functions_class()
        # #     # height, width = iterative_functions_obj.get_image_height_width(image_path+file_name)
        # #     # op_image_score = op_evaluation_obj.conduct_evaluation(file_name,height, width,output_img_path,op_path)
        # #
        #     #-----thesis calculations
        #     # door_precision, door_recall = self.calculate_precision_recall(d_true_positive, d_false_positive, d_gt_total)
        #     # window_precision, window_recall = self.calculate_precision_recall(w_true_positive, w_false_positive, w_gt_total)
        #     # text_precision,text_recall = self.calculate_precision_recall(t_true_positive, t_false_positive, t_gt_total)
        #     # st_precision, st_recall = self.calculate_precision_recall(st_true_positive, st_false_positive, st_gt_total)
        #     # room_precision, room_recall = self.calculate_precision_recall(room_tp, room_fp, r_gt_total)
        #
            #--TACCESS paper calculations
            # door_hc_precision, door_hc_recall,door_mc_precision, door_mc_recall = self.calculate_precision_recall_ppr(d_true_positive, d_false_positive, d_gt_total,d_HC_count, d_MC_count)
            # window_hc_precision, window_hc_recall, window_mc_precision, window_mc_recall = self.calculate_precision_recall_ppr(w_true_positive, w_false_positive, w_gt_total, w_HC_count, w_MC_count)
            # text_hc_precision, text_hc_recall, text_mc_precision, text_mc_recall = self.calculate_precision_recall_ppr(t_true_positive, t_false_positive, t_gt_total, t_HC_count, t_MC_count)
            # stairs_hc_precision, stairs_hc_recall, stairs_mc_precision, stairs_mc_recall = self.calculate_precision_recall_ppr(st_true_positive, st_false_positive, st_gt_total, st_HC_count, st_MC_count)
            # sgl_room_text_matched_tp, sgl_room_polygon_matched_tp, sgl_subarea_text_matched_tp, room_gt_total = self.calculate_sgl_room_data(sgl_room_text_matched, sgl_room_polygon_matched, sgl_subarea_text_matched)

            image_values = [file_name, d_true_positive,d_matched_HC_count,d_HC_count,d_matched_MC_count,d_MC_count,d_false_positive,d_gt_total,w_true_positive,w_matched_HC_count,w_HC_count,w_matched_MC_count,w_MC_count,w_false_positive,w_gt_total,t_true_positive,t_matched_HC_count,t_HC_count,t_matched_MC_count,t_MC_count,t_false_positive,t_gt_total,t_partial_sys_count,st_true_positive,st_matched_HC_count,st_HC_count,st_matched_MC_count,st_MC_count,st_false_positive,st_gt_total,gt_jaccard_index_walls, intersection_area_precision,intersection_area_recall,sys_wall_area, gt_wall_area,sgl_room_text_matched,sgl_room_polygon_matched-sgl_room_text_matched,sgl_subarea_text_matched,len(sys_json_data['sgl_rooms_list']),len(gt_sgl_rooms_list),op_room_text_matched,op_room_polygon_matched-op_room_text_matched,op_sgl_text_matched,len(sys_json_data['op_rooms_list']),gt_op_sub_area_count]

            # image_values = [file_name, d_true_positive, d_matched_HC_count, d_HC_count, d_matched_MC_count, d_MC_count,
            #                 d_false_positive, d_gt_total, w_true_positive, w_matched_HC_count, w_HC_count,
            #                 w_matched_MC_count, w_MC_count, w_false_positive, w_gt_total, t_true_positive,
            #                 t_matched_HC_count, t_HC_count, t_matched_MC_count, t_MC_count, t_false_positive,
            #                 t_gt_total, t_partial_sys_count, st_true_positive, st_matched_HC_count, st_HC_count,
            #                 st_matched_MC_count, st_MC_count, st_false_positive, st_gt_total, gt_jaccard_index_walls,
            #                 intersection_area_precision, intersection_area_recall, sys_wall_area, gt_wall_area]

            self.add_to_excel_columns(image_values,ws,row_count)
            wb.save(output_excl_path + 'Evaluation_Results.xls')

            row_count = row_count + 1

        wb.save(output_excl_path + 'Evaluation_Results.xls')

    def read_JSON_file(self, path,system_json):
        self.iterative_functions_obj = iterative_functions_class()
        json_data = {}
        sgl_rooms_list,op_rooms_list, wall_list, door_list,window_list,stair_list,text_list,unknown_list = [],[],[],[],[],[],[],[]
        with open(path) as json_file:
            data = json.load(json_file)
            for all_data in data:
                element_value = all_data['annotations']
                for element in element_value:
                    if system_json and element['class'] == 'Room':
                        # --get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get room cont cords
                        room_cords = []
                        for x_num, x in enumerate(int_x_list):
                            room_cords.append([x, int_y_list[x_num]])
                        room_type = str(element['type'])
                        text = str(element['text'])
                        text_centre_x = int(float(element['x']))
                        text_centre_y = int(float(element['y']))

                        # --add to room list
                        if room_type=='single':
                            sgl_rooms_list.append([text,[text_centre_x,text_centre_y],room_type,room_cords])
                        else:
                            op_rooms_list.append([text,[text_centre_x,text_centre_y],room_type,room_cords])


                    if element['class']=='Wall':
                        #--get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get wall cords
                        wall_cords = []
                        for x_num, x in enumerate(int_x_list):
                            wall_cords.append([x,int_y_list[x_num]])
                        if system_json:
                            #--get confidence
                            confidence = element['confidence']
                            # --add to wall list
                            wall_list.append([wall_cords,confidence])
                        else:
                            wall_list.append([wall_cords,1])

                    elif element['class']=='wall':
                        height = int(float(element['height']))
                        width = int(float(element['width']))
                        top_left_x = int(float(element['x']))
                        top_left_y = int(float(element['y']))
                        p1, p2, p3, p4 = self.iterative_functions_obj.find_points_rectangle(top_left_x,top_left_y,top_left_x+width,top_left_y+height)
                        wall_cords = [p1, p2, p3, p4]
                        if system_json:
                            # --get confidence
                            confidence = element['confidence']
                            # --add to wall list
                            wall_list.append([wall_cords, confidence])
                        else:
                            wall_list.append([wall_cords,1])

                    elif element['class']=='Door':
                        #--get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get door cords
                        door_cords = []
                        for x_num, x in enumerate(int_x_list):
                            door_cords.append([x,int_y_list[x_num]])
                        if system_json:
                            # --get confidence
                            confidence = element['confidence']
                            # --add to door list
                            door_list.append([door_cords,confidence])
                        else:
                            door_list.append([door_cords,1])

                    elif element['class']=='door':
                        height = int(float(element['height']))
                        width = int(float(element['width']))
                        top_left_x = int(float(element['x']))
                        top_left_y = int(float(element['y']))
                        p1, p2, p3, p4 = self.iterative_functions_obj.find_points_rectangle(top_left_x,top_left_y,top_left_x+width,top_left_y+height)
                        door_cords = [p1, p2, p3, p4]
                        if system_json:
                            # --get confidence
                            confidence = element['confidence']
                            # --add to door list
                            door_list.append([door_cords,confidence])
                        else:
                            door_list.append([door_cords,1])

                    elif element['class']=='Window':
                        #--get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get window cords
                        window_cords = []
                        for x_num, x in enumerate(int_x_list):
                            window_cords.append([x,int_y_list[x_num]])
                        if system_json:
                            # --get confidence
                            confidence = element['confidence']
                            # --add to window list
                            window_list.append([window_cords,confidence])
                        else:
                            window_list.append([window_cords,1])

                    elif element['class']=='window':
                        height = int(float(element['height']))
                        width = int(float(element['width']))
                        top_left_x = int(float(element['x']))
                        top_left_y = int(float(element['y']))
                        p1, p2, p3, p4 = self.iterative_functions_obj.find_points_rectangle(top_left_x,top_left_y,top_left_x+width,top_left_y+height)
                        window_cords = [p1, p2, p3, p4]
                        if system_json:
                            # --get confidence
                            confidence = element['confidence']
                            # --add to window list
                            window_list.append([window_cords,confidence])
                        else:
                            window_list.append([window_cords,1])

                    elif element['class']=='Stair' or element['class']=='Stairs':
                        #--get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get stair cords
                        stair_cords = []
                        for x_num, x in enumerate(int_x_list):
                            stair_cords.append([x,int_y_list[x_num]])
                        if system_json:
                            # --get confidence
                            confidence = element['confidence']
                            # --add to stair list
                            stair_list.append([stair_cords,confidence])
                        else:
                            # --add to stair list
                            stair_list.append([stair_cords, 1])

                    elif element['class']=='Text':
                        height = int(float(element['height']))
                        width = int(float(element['width']))
                        top_left_x = int(float(element['x']))
                        top_left_y = int(float(element['y']))
                        text = str(element['text'])
                        bottom_right_x, bottom_right_y = top_left_x+width, top_left_y+height
                        center_x, center_y = self.iterative_functions_obj.find_centre_of_line([[top_left_x,top_left_y], [bottom_right_x, bottom_right_y]])
                        #--use no numbers for evaluation
                        text_new = ''.join(x for x in text if x.isalpha())
                        if 'floor' in text_new:
                            continue

                        if system_json:
                            confidence = element['confidence']
                            text_list.append([text_new, height, [center_x, center_y],
                                              [top_left_x, top_left_y, bottom_right_x, bottom_right_y], 1])
                        else:
                            # --add to text_list
                            text_list.append([text_new,height,[center_x, center_y],[top_left_x,top_left_y, bottom_right_x, bottom_right_y],1])

                    elif element['class']=='Unknown':
                        #--get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get window cords
                        unknown_cords = []
                        for x_num, x in enumerate(int_x_list):
                            unknown_cords.append([x,int_y_list[x_num]])
                        # --add to window list
                        unknown_list.append(unknown_cords)

        json_data['sgl_rooms_list'] = sgl_rooms_list
        json_data['op_rooms_list'] = op_rooms_list
        json_data['wall_list'] = wall_list
        json_data['door_list'] = door_list
        json_data['window_list'] = window_list
        json_data['stair_list'] = stair_list
        json_data['text_list'] = text_list
        json_data['unknown_list'] = unknown_list

        return json_data

    def read_gt_room_JSON_file(self,path,cvc):
        sgl_rooms_list, op_rooms_list = [],[]
        with open(path) as json_file:
            data = json.load(json_file)
            for all_data in data:
                element_value = all_data['annotations']
                for element in element_value:
                    if element['class'] == 'Room':
                        # --get xn values
                        x_str_list = re.findall(r"[\w.']+", element['xn'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['yn'])
                        int_y_list = [int(float(x)) for x in y_str_list]
                        # --get room cont cords
                        room_cords = []
                        for x_num, x in enumerate(int_x_list):
                            room_cords.append([x, int_y_list[x_num]])
                        room_type = str(element['type'])
                        #---get texts
                        text = str(element['text'])
                        text_list = text.split(';')[:-1]
                        # --get xn values
                        x_str_list = re.findall(r"[\w.']+", element['x'])
                        int_x_list = [int(float(x)) for x in x_str_list]
                        # --get yn values
                        y_str_list = re.findall(r"[\w.']+", element['y'])
                        int_y_list = [int(float(x)) for x in y_str_list]

                        text_data = []
                        for r,each_text in enumerate(text_list):
                            if cvc: new_text = self.iterative_functions_obj.match_text_to_rooms(each_text)
                            else: new_text = each_text
                            text_data.append([new_text,[int_x_list[r],int_y_list[r]]])

                        # --add to rooms list
                        if room_type=='single':
                            sgl_rooms_list.append([text_data[0][0],text_data[0][1],room_type,room_cords])
                        else:
                            op_rooms_list.append([text_data, room_type, room_cords])

        return sgl_rooms_list, op_rooms_list

    def count_gt_op_sub_areas(self,gt_op_rooms_list):
        total_gt_op_sub_areas = 0
        if len(gt_op_rooms_list)>0:
            for row in gt_op_rooms_list:
                text_data = row[0][0]
                total_gt_op_sub_areas = total_gt_op_sub_areas + len(text_data)
        return total_gt_op_sub_areas


    def calculate_precision_recall(self,true_positive, false_positive, gt_total):
        if true_positive>0 or false_positive>0:
            precision = round((true_positive / float(true_positive + false_positive)), 2)
        elif gt_total>0:
            precision = 0
        else:
            precision = 'NA'

        if gt_total>0:
            recall = round((true_positive / float(gt_total)), 2)
        else:
            recall = 'NA'

        return precision,recall

    def calculate_precision_recall_ppr(self,true_positive, false_positive, gt_total,HC_count, MC_count):
        if true_positive>0 or false_positive>0:
            hc_precision = round((HC_count / float(true_positive + false_positive)), 2)
            mc_precision = round((MC_count / float(true_positive + false_positive)), 2)
        elif gt_total>0:
            hc_precision,mc_precision = 0,0
        else:
            hc_precision, mc_precision = 'NA', 'NA'

        if gt_total>0:
            hc_recall = round((HC_count / float(gt_total)), 2)
            mc_recall = round((MC_count / float(gt_total)), 2)
        else:
            hc_recall, mc_recall = 'NA','NA'

        return hc_precision, hc_recall,mc_precision, mc_recall

    def calculate_sgl_room_data(self,sgl_room_text_matched, sgl_room_polygon_matched, sgl_subarea_text_matched):
        pass

    def write_excel_row(self,row_count,col_count,value,ws):
        # if value == 0:
        #     ws.write(row_count, col_count, 'NA')
        # else:
        #     ws.write(row_count, col_count, value)
        # return ws
        ws.write(row_count, col_count, value)
        return ws

    def add_to_excel_columns(self, values, ws, row_number):
        for col_number, value in enumerate(values):
            ws.write(row_number, col_number, value)
        return ws





















if __name__ == '__main__':
    hwl1 = MainFormLogic()