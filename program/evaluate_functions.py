import re, cv2, os, random
import numpy as np
from PIL import Image

from iterative_functions import iterative_functions_class

class evaluate_functions_class:

    def __init__(self):
        self.iterative_obj = iterative_functions_class()

    def extract_poplar_room_data(self, path_gravvitas, only_image_name):
        # --find lines for all rooms and store in "room_lines_list[room_name,room_contour_lines_list]"
        all_op_data = []
        room_details_file = open(path_gravvitas + only_image_name + '_8_openPlan_details.txt', 'r')
        # print path_gravvitas + only_image_name + '_8_openPlan_details.txt---------------------'
        for op_number, text_line in enumerate(room_details_file):
            contour_seperate = text_line.split(':')
            if len(contour_seperate) > 3:
                all_room_data = contour_seperate[5:]
                room_data_list = [all_room_data[i:i + 4] for i in range(0, len(all_room_data), 4)]
                op_room_data = []
                for row in room_data_list:
                    # --extract text label and text codinate
                    text_label = row[0].strip()
                    if 'maybe' in text_label:
                        new_text_label = text_label.replace('maybe ','')
                        print 'maybe was in. Resetting ', text_label, ' to ', new_text_label
                    else:
                        new_text_label = text_label
                    text_cord_details = re.findall(r"[\w']+", row[1])
                    text_cord = [int(x) for x in text_cord_details]
                    # ---extract room contour
                    contour_details = re.findall(r"[\w']+", row[3])
                    int_contour_details = [int(x) for x in contour_details]
                    chunks = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                    add_bracket = []
                    for each_chunk in chunks:
                        add_bracket.append([each_chunk])
                    room_contour = np.asarray(add_bracket)
                    op_room_data.append([new_text_label, text_cord, room_contour])
                    # print 'text_label,text_cord',text_label,text_cord
                all_op_data.append([op_number, op_room_data])
        return all_op_data

    def extract_poplar_op_data(self, path_gravvitas, only_image_name, orginal_img_height, orginal_img_width,
                               evaluation_output_path):
        # --find lines for all rooms and store in "room_lines_list[room_name,room_contour_lines_list]"
        op_data = []
        op_details_file = open(path_gravvitas + only_image_name + '_8_openPlan_details.txt', 'r')
        for text_line in op_details_file:
            contour_seperate = text_line.split(':')
            if len(contour_seperate) > 2:
                # --openplan number
                op_num = re.findall('\d+', contour_seperate[0])
                op_number = [int(x) for x in op_num]
                # ---extract room contour
                contour_details = re.findall(r"[\w']+", contour_seperate[2])
                int_contour_details = [int(x) for x in contour_details]
                chunks = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                add_bracket = []
                for each_chunk in chunks:
                    add_bracket.append([each_chunk])
                op_contour = np.asarray(add_bracket)
                # ---extract partition lines
                line_details = re.findall(r"[\w']+", contour_seperate[4])
                int_line_details = [int(x) for x in line_details]
                line_chunks = [int_line_details[x:x + 2] for x in xrange(0, len(int_line_details), 2)]
                partition_lines = [line_chunks[x:x + 2] for x in xrange(0, len(line_chunks), 2)]

                # pop_contour_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                # cv2.drawContours(pop_contour_img, [op_contour], 0, (0, 0, 0), -1, cv2.CV_AA)
                # cv2.imwrite(evaluation_output_path + '_0out_cont.png', pop_contour_img)
                op_data.append([op_number, op_contour, partition_lines])
        return op_data

    def extract_gt_room_data(self, path_gt):
        gt_image_list = os.listdir(path_gt)
        gt_room_data = []
        for gt_file_name in gt_image_list:
            # --find area GTs
            if '_A' in gt_file_name:
                single_GT = []
                gt_details_file = open(path_gt + gt_file_name, 'r')
                for text_line in gt_details_file:
                    contour_seperate = text_line.split(':')
                    if len(contour_seperate) > 2:
                        # --extract text label and text codinate
                        text_label = contour_seperate[2].strip()
                        text_label_new = self.iterative_obj.match_text_to_rooms(text_label)
                        if text_label_new=='':
                            continue
                        text_cord_details = re.findall(r"[\w']+", contour_seperate[3])
                        text_cord = [int(x) for x in text_cord_details]
                        # ---extract room contour
                        contour_details = re.findall(r"[\w']+", contour_seperate[1])
                        int_contour_details = [int(x) for x in contour_details]
                        chunks = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                        add_bracket = []
                        for each_chunk in chunks:
                            add_bracket.append([each_chunk])
                        room_contour = np.asarray(add_bracket)
                        # print 'text_label', text_label
                        single_GT.append([text_label_new, text_cord, room_contour])
                if len(single_GT) > 0:
                    gt_room_data.append(single_GT)
        return gt_room_data




    def extract_gt_line_data(self, path_gt):
        gt_image_list = os.listdir(path_gt)
        gt_line_data = []
        for gt_file_name in gt_image_list:
            # --find area GTs
            if '_L' in gt_file_name:
                GT_lines = []
                gt_details_file = open(path_gt + gt_file_name, 'r')
                for text_line in gt_details_file:
                    contour_seperate = text_line.split(':')
                    if len(contour_seperate) > 1:
                        # ---extract room contour
                        contour_details = re.findall(r"[\w']+", contour_seperate[1])
                        int_contour_details = [int(x) for x in contour_details]
                        chunks = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                        add_bracket = []
                        for each_chunk in chunks:
                            add_bracket.append([each_chunk])
                        line_contour = np.asarray(add_bracket)
                        GT_lines.append(line_contour)
                if len(GT_lines) > 0:
                    gt_line_data.append(GT_lines)
        return gt_line_data

    def find_gt_for_rooms(self, all_op_data, gt_room_data):
        op_area_list = []
        for op_num, op_plan in enumerate(all_op_data):
            area_data = []
            for system_row in op_plan[1]:
                # print system_row[0],system_row[1]
                pop_contour = system_row[2]
                pop_x, pop_y = system_row[1]
                pop_label = system_row[0].strip().lower()
                gt_rooms_data = []
                # print 'pop_label',pop_label
                for gt_row in gt_room_data:
                    for gt_rooms in gt_row:
                        gt_x, gt_y = gt_rooms[1]
                        gt_label = gt_rooms[0].strip().lower()
                        # print 'gt_label',gt_label
                        if (abs(gt_x - pop_x) < 110 and abs(gt_y - pop_y) < 110) and pop_label == gt_label:
                            gt_rooms_data.append(gt_rooms[2])
                            # print 'INSIDE gt_label', gt_label
                            break
                area_data.append([[pop_x, pop_y], pop_contour, gt_rooms_data])
            op_area_list.append([op_num, area_data])
        return op_area_list

    def find_gt_for_lines(self, op_data, gt_line_data):
        line_list = []
        for system_row in op_data:
            op_number = system_row[0]
            op_contour = system_row[1]
            op_partition_lines = system_row[2]
            line_list.append([op_number, op_contour, op_partition_lines, gt_line_data])
        return line_list

    def compare_poplar_and_gt_JI(self, orginal_img_height, orginal_img_width, op_area_list, evaluation_output_path):
        # ---generate intermediate results
        # ji_sys_rooms_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
        # color = [[0, 0, 255], [0, 255, 0], [255, 0, 0], [255, 255, 0], [0, 150, 0], [150, 0, 255], [128, 0, 255],
        #          [0, 128, 255], [0, 0, 0]]
        # for op_num, op_plan_row in enumerate(op_area_list):
        #     for result_num,area_row in enumerate(op_plan_row[1]):
        #         pop_contour = area_row[1]
        #         color_val = tuple(random.choice(color))
        #         cv2.drawContours(ji_sys_rooms_img, [pop_contour], 0, color_val, 3, cv2.CV_AA)
        #     cv2.imwrite(evaluation_output_path + 'JI_Area_Sys_Rooms.png', ji_sys_rooms_img)
        # --end intermediate results

        op_JI = []
        for op_num, op_plan_row in enumerate(op_area_list):
            for room_num, area_row in enumerate(op_plan_row[1]):
                pop_contour = area_row[1]
                pop_contour_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                cv2.drawContours(pop_contour_img, [pop_contour], 0, (0, 0, 0), -1, cv2.CV_AA)
                # --intermediate result - commentable
                # cv2.imwrite(evaluation_output_path + 'JI_Room_'+str(room_num)+'.png', pop_contour_img)

                room_JI = []
                # --write GT to images
                for gt_num, gt_row in enumerate(area_row[2]):
                    gt_contour = gt_row
                    gt_contour_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                    cv2.drawContours(gt_contour_img, [gt_contour], 0, (0, 0, 0), -1, cv2.CV_AA)
                    # --intermediate result - commentable
                    # cv2.imwrite(evaluation_output_path + 'JI_Room_'+str(room_num)+'_GT_'+str(gt_num)+'.png', gt_contour_img)

                    # --inverse gt and pop results to get AND, OR results
                    gt_contour_inv = cv2.bitwise_not(gt_contour_img)
                    pop_contour_inv = cv2.bitwise_not(pop_contour_img)

                    # --get AND bewteen GT and POP
                    contour_AND_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                    cv2.bitwise_and(pop_contour_inv, gt_contour_inv, contour_AND_img, mask=None)
                    # --intermediate result - commentable
                    # cv2.imwrite(evaluation_output_path + 'JI_Room_'+str(room_num)+'_GT_'+str(gt_num)+'AND_Result.png',contour_AND_img)

                    # --get OR bewteen GT and POP
                    contour_OR_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                    cv2.bitwise_or(pop_contour_inv, gt_contour_inv, contour_OR_img, mask=None)
                    # --intermediate result - commentable
                    # cv2.imwrite(evaluation_output_path + 'JI_Room_'+str(room_num)+'_GT_'+str(gt_num)+'OR_Result.png',contour_OR_img)

                    # ----get contours from AND images and find 'intersection_area'
                    gray_contour_AND_img = cv2.cvtColor(contour_AND_img, cv2.COLOR_BGR2GRAY)
                    ret, thresh = cv2.threshold(gray_contour_AND_img, 127, 255, 1)
                    and_contours, hierachy = cv2.findContours(thresh, 1, 2)
                    intersection_area = cv2.contourArea(and_contours[0])

                    # ----get contours from OR images and find 'union_area'
                    gray_contour_OR_img = cv2.cvtColor(contour_OR_img, cv2.COLOR_BGR2GRAY)
                    ret, thresh = cv2.threshold(gray_contour_OR_img, 127, 255, 1)
                    or_contours, hierachy = cv2.findContours(thresh, 1, 2)
                    union_area = cv2.contourArea(or_contours[0])

                    # print room_num, 'intersection_area',intersection_area
                    # print room_num, 'union_area' , union_area

                    # --do calculations
                    gt_jaccard_index = round((intersection_area / float(union_area)), 3)
                    room_JI.append(gt_jaccard_index)
                    # print 'gt_jaccard_index',gt_jaccard_index

                    # #--create intermediate results
                    # intermediate_result_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                    # cv2.drawContours(intermediate_result_img, [and_contours[0]], 0, (190, 190, 190), -1, cv2.CV_AA)
                    # cv2.drawContours(intermediate_result_img, [pop_contour], 0, (0, 0, 255), 3, cv2.CV_AA)
                    # cv2.drawContours(intermediate_result_img, [gt_contour], 0, (255, 0, 0), 3, cv2.CV_AA)
                    # cv2.imwrite(
                    #     evaluation_output_path + 'JI_Room_' + str(room_num) + '_GT_' + str(gt_num) + 'Overlap.png',
                    #     intermediate_result_img)
                if len(room_JI)>0:
                    average_room_JI = sum(room_JI) / len(room_JI)
                    op_JI.append(average_room_JI)
                # print 'sum(room_JI)',sum(room_JI)
                # print 'len(room_JI)',len(room_JI)
                # print 'average_room_JI',average_room_JI

        average_op_JI = round(sum(op_JI) / len(op_JI), 3)
        # print 'sum(op_JI)',sum(op_JI)
        # print 'len(op_JI)',len(op_JI)
        # print 'average_op_JI',average_op_JI

        return average_op_JI

    def compare_poplar_and_gt_CR(self, orginal_img_height, orginal_img_width, op_area_list, evaluation_output_path):
        op_CR = []
        for op_num, op_plan_row in enumerate(op_area_list):
            for room_num, area_row in enumerate(op_plan_row[1]):
                # if result_num==0:
                pop_contour = area_row[1]
                pop_contour_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                cv2.drawContours(pop_contour_img, [pop_contour], 0, (0, 0, 0), -1, cv2.CV_AA)
                # cv2.imwrite(evaluation_output_path + 'CR_Room_' + str(room_num) + '.png', pop_contour_img)

                core_region_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                # --write GT to images
                for gt_num, gt_row in enumerate(area_row[2]):
                    gt_contour = gt_row
                    gt_contour_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                    cv2.drawContours(gt_contour_img, [gt_contour], 0, (0, 0, 0), -1, cv2.CV_AA)
                    # cv2.imwrite(evaluation_output_path + 'CR_Room_' + str(room_num) + '_GT_' + str(gt_num) + '.png',gt_contour_img)

                    # --inverse gt
                    gt_contour_inv = cv2.bitwise_not(gt_contour_img)
                    # --find core area by 'AND' GT and old 'core_region_img'
                    cv2.bitwise_and(core_region_img, gt_contour_inv, core_region_img, mask=None)

                # cv2.imwrite(evaluation_output_path + 'CR_Room_' + str(room_num) + '_GT_Core_Region.png',core_region_img)

                # --inverse system result
                sys_contour_inv = cv2.bitwise_not(pop_contour_img)
                # cv2.imwrite(evaluation_output_path + 'CR_Room_' + str(room_num) + '-GT_sysResltInverse.png',sys_contour_inv)

                # --find intersection between 'core_region_img' and 'System result'
                sys_AND_core_region = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                cv2.bitwise_and(core_region_img, sys_contour_inv, sys_AND_core_region, mask=None)
                # cv2.imwrite(evaluation_output_path + 'CR_Room_' + str(room_num) + '-3_CoreSysANDResult.png',sys_AND_core_region)

                # ----get contours from OR images and find 'union_area'
                gray_sys_AND_core_region = cv2.cvtColor(sys_AND_core_region, cv2.COLOR_BGR2GRAY)
                ret, thresh = cv2.threshold(gray_sys_AND_core_region, 127, 255, 1)
                sys_AND_core_contours, hierachy = cv2.findContours(thresh, 1, 2)
                sys_AND_core_area = cv2.contourArea(sys_AND_core_contours[0])

                # --find contours in 'core_region_img'
                gray_core_region_img = cv2.cvtColor(core_region_img, cv2.COLOR_BGR2GRAY)
                ret, thresh = cv2.threshold(gray_core_region_img, 127, 255, 1)
                core_contours, hierachy = cv2.findContours(thresh, 1, 2)
                if len(core_contours)>0:
                    core_region_area = cv2.contourArea(core_contours[0])

                    room_core_region_ratio = sys_AND_core_area / float(core_region_area)
                    op_CR.append(room_core_region_ratio)
                    # print '--',room_num,'-----'
                    # print 'core_region_area',core_region_area
                    # print 'sys_AND_core_area',sys_AND_core_area
                    # print 'room_core_region_ratio',room_core_region_ratio

                    # #--intermediate results-deletable
                    # intermediate_result_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                    # cv2.drawContours(intermediate_result_img, [core_contours[0]], 0, (160, 160, 160), -1, cv2.CV_AA)
                    # for gt_num, gt_row in enumerate(area_row[2]):
                    #     gt_contour = gt_row
                    #     cv2.drawContours(intermediate_result_img, [gt_contour], 0, (255, 0, 0), 2, cv2.CV_AA)
                    # cv2.imwrite(evaluation_output_path + 'CR_Room_' + str(room_num) + '_GT_CoreArea.png',
                    #             intermediate_result_img)
                    #
                    # # --create intermediate results
                    # intermediate_result_img2 = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
                    # cv2.drawContours(intermediate_result_img2, [sys_AND_core_contours[0]], 0, (160, 160, 160), -1, cv2.CV_AA)
                    # cv2.drawContours(intermediate_result_img2, [pop_contour], 0, (0, 0, 255), 3, cv2.CV_AA)
                    # cv2.drawContours(intermediate_result_img2, [core_contours[0]], 0, (255, 0, 0), 3, cv2.CV_AA)
                    # cv2.imwrite(
                    #     evaluation_output_path + 'CR_Room_' + str(room_num) + '_GT_CoreAreaANDSystem.png',
                    #     intermediate_result_img2)

        average_op_CR = round(sum(op_CR) / len(op_CR), 3)
        # print 'sum(op_CR)',sum(op_CR)
        # print 'len(op_CR)',len(op_CR)
        # print 'average_op_CR',average_op_CR

        return average_op_CR

    def compare_poplar_and_gt_lines(self, orginal_img_height, orginal_img_width, line_list, evaluation_output_path):
        op_pixel_ratio = 0
        for open_plan_num, line_row in enumerate(line_list):
            comparison_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
            # op_contour = line_row[1]
            op_partition_lines = line_row[2]

            # #--generate intermediate results
            # intermediate_result = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
            # cv2.drawContours(intermediate_result, [op_contour], 0, (0, 0, 0), 3, cv2.CV_AA)

            for op_line in op_partition_lines:
                # cv2.line(intermediate_result, (tuple(op_line[0])), (tuple(op_line[1])), (0, 0, 255), 5, cv2.cv.CV_AA)
                cv2.line(comparison_img, (tuple(op_line[0])), (tuple(op_line[1])), (0, 0, 255), 5, cv2.cv.CV_AA)
            # cv2.imwrite(evaluation_output_path + 'LPR_Room_' + str(open_plan_num) + '.png', intermediate_result)

            cv2.imwrite(evaluation_output_path + str(open_plan_num) + 'sys_result.png', comparison_img)
            sys_image = Image.open(evaluation_output_path + str(open_plan_num) + 'sys_result.png')
            sys_red_pixel_count = 0
            for sys_pixel in sys_image.getdata():
                if sys_pixel == (255, 0, 0):
                    sys_red_pixel_count += 1

            # comparison_img = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))

            op_pixel_ratio = []
            # --write GT to images
            for gt_num, gt_row in enumerate(line_row[3]):
                # op_partition_lines = line_row[2]
                # for op_line in op_partition_lines:
                #     cv2.line(comparison_img, (tuple(op_line[0])), (tuple(op_line[1])), (0, 0, 255), 5, cv2.cv.CV_AA)

                for gt_contour_line in gt_row:
                    cv2.drawContours(comparison_img, [gt_contour_line], 0, (0, 0, 0), -1, cv2.CV_AA)
            cv2.imwrite(evaluation_output_path + 'LPR_Room_' + str(open_plan_num) + '_GT_' + str(gt_num) + '.png',
                        comparison_img)

            gt_im = Image.open(
                evaluation_output_path + 'LPR_Room_' + str(open_plan_num) + '_GT_' + str(gt_num) + '.png')
            left_over_red_px_count = 0
            for gt_pixel in gt_im.getdata():
                if gt_pixel == (255, 0, 0):
                    left_over_red_px_count += 1

            sys_good_pixel = sys_red_pixel_count - left_over_red_px_count
            op_pixel_ratio = sys_good_pixel / float(sys_red_pixel_count)
            # op_pixel_ratio.append(gt_pixel_ratio)

            # print 'sys_red_pixel_count', sys_red_pixel_count
            # print 'left_over_red_px_count', left_over_red_px_count
            # print 'sys_good_pixel', sys_good_pixel
            # print 'op_pixel_ratio', op_pixel_ratio

            # average_op_pixel_ratio = round(sum(op_pixel_ratio)/len(op_pixel_ratio),3)
            # print 'sum(op_pixel_ratio)',sum(op_pixel_ratio)
            # print 'len(op_pixel_ratio)',len(op_pixel_ratio)
            # print 'average_op_pixel_ratio',average_op_pixel_ratio

        return op_pixel_ratio

