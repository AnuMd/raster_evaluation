import cv2, numpy as np,math,traceback,os,re
from shapely.geometry import Polygon
from PIL import Image


from iterative_functions import iterative_functions_class

class find_missing_class:
    def __init__(self):
        self.iterative_functions_obj = iterative_functions_class()

    def find_missing_GT_elements(self,sys_json_data,gt_json_data,output_path,input_image_path,unknown):
        input_image = cv2.imread(input_image_path,cv2.IMREAD_COLOR)
        height, width, depth = input_image.shape

        gt_wall_list, gt_door_list, gt_window_list, gt_stair_list, gt_text_list,gt_unknown_list = self.extract_data(gt_json_data,'gt')
        sys_wall_list, sys_door_list, sys_window_list, sys_stair_list, sys_text_list,sys_unknown_list = self.extract_data(sys_json_data,'sys')
        if len(sys_text_list)>0:
            text_height = int(round((sum([text_row[1] for text_row in sys_text_list])/float(len(sys_text_list)))))
        else:
            text_height = 3


        #--doors
        sys_door_list, gt_door_list,repeat_count,ignorable_sys_cont_count = self.find_missing_door_window(gt_door_list, sys_door_list,sys_unknown_list,output_path,input_image,text_height,'door',unknown)
        d_true_positive, d_false_positive, d_gt_total,partial_sys_count,d_HC_count,d_MC_count,d_matched_HC_count,d_matched_MC_count = self.calculate_detection(sys_door_list, gt_door_list,'door',repeat_count,ignorable_sys_cont_count)

        #--windows
        sys_window_list, gt_window_list,repeat_count,ignorable_sys_cont_count = self.find_missing_door_window(gt_window_list, sys_window_list, sys_unknown_list,output_path, input_image, text_height,'window',unknown)
        w_true_positive, w_false_positive, w_gt_total,partial_sys_count,w_HC_count,w_MC_count,w_matched_HC_count,w_matched_MC_count = self.calculate_detection(sys_window_list, gt_window_list, 'window',repeat_count,ignorable_sys_cont_count)

        # -- text
        sys_text_list, gt_text_list = self.find_missing_text_labels(gt_text_list, sys_text_list, text_height,input_image, output_path)
        t_true_positive, t_false_positive, t_gt_total,t_partial_sys_count,t_HC_count,t_MC_count,t_matched_HC_count,t_matched_MC_count = self.calculate_detection(sys_text_list, gt_text_list,'text',repeat_count=0,ignorable_sys_cont_count=0)

        #--walls
        # print 'walls'
        gt_jaccard_index_walls, sys_wall_area, gt_wall_area, intersection_area_precision,intersection_area_recall = self.calculate_JI(gt_wall_list,sys_wall_list,sys_unknown_list,output_path,input_image,height, width,'walls',unknown)

        #--stairs
        # print 'stairs'
        sys_stair_list, gt_stair_list = self.find_stairs_detection(gt_stair_list, sys_stair_list, sys_unknown_list,output_path, input_image,height, width)
        st_true_positive, st_false_positive, st_gt_total,partial_sys_count,st_HC_count,st_MC_count,st_matched_HC_count,st_matched_MC_count = self.calculate_detection(sys_stair_list, gt_stair_list, 'stairs',repeat_count=0,ignorable_sys_cont_count=0)

        # gt_jaccard_index_stairs = 0

        # d_true_positive, d_false_positive, d_gt_total, w_true_positive, w_false_positive, w_gt_total, t_true_positive, t_false_positive, t_gt_total,st_true_positive, st_false_positive, st_gt_total, t_partial_sys_count= 0,0,0,0,0,0,0,0,0,0,0,0,0

        # t_true_positive, t_false_positive, t_gt_total, t_partial_sys_count, w_true_positive, w_false_positive, w_gt_total, gt_jaccard_index_walls,st_true_positive, st_false_positive, st_gt_total,d_true_positive, d_false_positive,d_gt_total= 0,0,0,0,0,0,0,0,0,0,0,0,0,0

        d_data, w_data, t_data, st_data,wl_data = [d_true_positive, d_false_positive, d_gt_total,partial_sys_count,d_HC_count,d_MC_count,d_matched_HC_count,d_matched_MC_count], [w_true_positive, w_false_positive, w_gt_total,partial_sys_count,w_HC_count,w_MC_count,w_matched_HC_count,w_matched_MC_count], [t_true_positive, t_false_positive, t_gt_total,t_partial_sys_count,t_HC_count,t_MC_count,t_matched_HC_count,t_matched_MC_count], [st_true_positive, st_false_positive, st_gt_total,partial_sys_count,st_HC_count,st_MC_count,st_matched_HC_count,st_matched_MC_count],[gt_jaccard_index_walls, sys_wall_area, gt_wall_area, intersection_area_precision,intersection_area_recall]

        return d_data, w_data, t_data, st_data, wl_data


    def extract_data(self,json_data,type):
        wall_list = json_data['wall_list']
        door_list = json_data['door_list']
        window_list = json_data['window_list']
        stair_list = json_data['stair_list']
        text_list = json_data['text_list']
        if type=='sys':
            unknown_list = json_data['unknown_list']
        else:
            unknown_list = []
        return wall_list,door_list,window_list,stair_list,text_list,unknown_list


    def find_missing_door_window(self,gt_list, sys_list,unknown_list,output_path,input_image,text_height,element_type,unknown):
        image = input_image.copy()

        #-- a pre-processing step
        for r,row in enumerate(sys_list):
            sys_list[r]= [row] +['NM']

        for r,row in enumerate(gt_list):
            gt_list[r]= [row] +['NM']

        for r,row in enumerate(unknown_list):
            if 'NM' not in row and 'M' not in row:
                unknown_list[r]= [row] +['NM']

        repeat_count,ignorable_sys_cont_count = 0,0
        # print element_type
        #--step 1: match with doors/windows
        if element_type=='door':
            repeat_count, ignorable_sys_cont_count, image = self.compare_system_detection_with_gts_door(sys_list,gt_list,image,repeat_count,text_height,ignorable_sys_cont_count,0.01)
        else:
            repeat_count, ignorable_sys_cont_count, image = self.compare_system_detection_with_gts_win(sys_list, gt_list, image, repeat_count,text_height, ignorable_sys_cont_count,0.1)
        # --step 2: match with unknown elements
        # print 'unknown'
        if unknown:
            repeat_count, image = self.compare_unknown_detection_with_gts(gt_list,unknown_list,image,repeat_count,text_height,0.1)

        cv2.imwrite(output_path +'_'+element_type+ '_matched.png', image)

        return sys_list, gt_list, repeat_count,ignorable_sys_cont_count

    def compare_system_detection_with_gts_door(self,sys_list,gt_list,image,repeat_count,text_height,ignorable_sys_cont_count,threshold):
        for sys_row_num, system_row in enumerate(sys_list):
            system_detection = system_row[0][0]
            system_cnt = self.iterative_functions_obj.convert_points_to_contour(system_detection)
            #---if contour is a straight line
            if cv2.contourArea(system_cnt) == 0:
                ignorable_sys_cont_count = ignorable_sys_cont_count + 1
            else:
                sys_cx, sys_cy = self.iterative_functions_obj.get_centre_of_contour(system_cnt)
                cv2.drawContours(image, [system_cnt], -1, (0, 0, 255), 3)

                for gt_row_num, gt_row in enumerate(gt_list):
                    gt_detection = gt_row[0][0]
                    gt_cnt = self.iterative_functions_obj.convert_points_to_contour(gt_detection)
                    gt_cx, gt_cy = self.iterative_functions_obj.get_centre_of_contour(gt_cnt)
                    cv2.drawContours(image, [gt_cnt], -1, (0, 255, 0), 3)


                    distance_matched = self.measure_distance_between_centre_points(sys_cx, sys_cy,gt_cx, gt_cy,text_height,'door')

                    if distance_matched:
                        cv2.circle(image, (sys_cx, sys_cy), 15, (255, 0, 0), -1)
                        cv2.circle(image, (sys_cx, sys_cy), 15, (0, 255, 255), 2)
                        # cv2.putText(image, str(ji), (sys_cx - 20, sys_cy - 30),cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 3, cv2.CV_AA)
                        # cv2.putText(image, str(repeat_count), (sys_cx - 30, sys_cy -40), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0), 3, cv2.CV_AA)
                        # --some doors/windows might be detected by system twice, so we check if already matched with gt to reduce repeated count
                        if gt_list[gt_row_num][1] == 'M':
                            repeat_count = repeat_count + 1
                            sys_list[sys_row_num][1] = 'M'
                        else:
                            sys_list[sys_row_num][1] = 'M'
                            gt_list[gt_row_num][1] = 'M'
                        # # ---1. get intersection
                        # try:
                        #     ji = self.calculate_intersection_area(system_cnt, gt_cnt,False)
                        #     # print ji
                        #     if ji > threshold:
                        #         cv2.circle(image, (sys_cx, sys_cy), 15, (255, 0, 0), -1)
                        #         cv2.circle(image, (sys_cx, sys_cy), 15, (0, 255, 255), 2)
                        #         # cv2.putText(image, str(ji), (sys_cx - 20, sys_cy - 30),cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 3, cv2.CV_AA)
                        #         # cv2.putText(image, str(repeat_count), (sys_cx - 30, sys_cy -40), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0), 3, cv2.CV_AA)
                        #         # --some doors/windows might be detected by system twice, so we check if already matched with gt to reduce repeated count
                        #         if gt_list[gt_row_num][1] == 'M':
                        #             repeat_count = repeat_count + 1
                        #             sys_list[sys_row_num][1] = 'M'
                        #         else:
                        #             sys_list[sys_row_num][1] = 'M'
                        #             gt_list[gt_row_num][1] = 'M'
                        #         # break
                        # except:
                        #     print "---------------------Error occured so skipping"
                        #     traceback.print_exc()

        return repeat_count, ignorable_sys_cont_count, image

    def compare_system_detection_with_gts_win(self,sys_list,gt_list,image,repeat_count,text_height,ignorable_sys_cont_count,threshold):
        for sys_row_num, system_row in enumerate(sys_list):
            system_detection = system_row[0][0]
            system_cnt = self.iterative_functions_obj.convert_points_to_contour(system_detection)
            #---if contour is a straight line
            if cv2.contourArea(system_cnt) == 0:
                ignorable_sys_cont_count = ignorable_sys_cont_count + 1
            else:
                sys_cx, sys_cy = self.iterative_functions_obj.get_centre_of_contour(system_cnt)
                cv2.drawContours(image, [system_cnt], -1, (0, 0, 255), 3)

                for gt_row_num, gt_row in enumerate(gt_list):
                    gt_detection = gt_row[0][0]
                    gt_cnt = self.iterative_functions_obj.convert_points_to_contour(gt_detection)
                    gt_cx, gt_cy = self.iterative_functions_obj.get_centre_of_contour(gt_cnt)
                    cv2.drawContours(image, [gt_cnt], -1, (0, 255, 0), 3)


                    distance_matched = self.measure_distance_between_centre_points(sys_cx, sys_cy,gt_cx, gt_cy,text_height,'win')

                    if distance_matched:
                        # ---1. get intersection
                        try:
                            ji,int_area = self.calculate_intersection_area(system_cnt, gt_cnt,True,op=False)
                            # print ji
                            if ji > threshold:
                                cv2.circle(image, (sys_cx, sys_cy), 15, (255, 0, 0), -1)
                                cv2.circle(image, (sys_cx, sys_cy), 15, (0, 255, 255), 2)
                                # cv2.putText(image, str(ji), (sys_cx - 20, sys_cy - 30),cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 3, cv2.CV_AA)
                                # cv2.putText(image, str(repeat_count), (sys_cx - 30, sys_cy -40), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0), 3, cv2.CV_AA)
                                # --some doors/windows might be detected by system twice, so we check if already matched with gt to reduce repeated count
                                if gt_list[gt_row_num][1] == 'M':
                                    repeat_count = repeat_count + 1
                                    sys_list[sys_row_num][1] = 'M'
                                else:
                                    sys_list[sys_row_num][1] = 'M'
                                    gt_list[gt_row_num][1] = 'M'
                                # break
                        except:
                            print "---------------------Error occured so skipping"
                            traceback.print_exc()

        return repeat_count, ignorable_sys_cont_count, image


    def compare_unknown_detection_with_gts(self,gt_list,unknown_list,image,repeat_count,text_height,threshold):
        for gt_row_num, gt_row in enumerate(gt_list):
            if gt_row[-1]=='NM':
                gt_detection = gt_row[0]
                gt_cnt = self.iterative_functions_obj.convert_points_to_contour(gt_detection)
                gt_cx, gt_cy = self.iterative_functions_obj.get_centre_of_contour(gt_cnt)
                cv2.drawContours(image, [gt_cnt], -1, (0, 255, 0), 3)

                for uknown_r_num,unknown_row in enumerate(unknown_list):
                    unknown_detection = unknown_row[0]
                    unknown_cnt = self.iterative_functions_obj.convert_points_to_contour(unknown_detection)
                    # ---if contour is a straight line
                    if cv2.contourArea(unknown_cnt) >0:
                        unknown_cx, unknown_cy = self.iterative_functions_obj.get_centre_of_contour(unknown_cnt)
                        cv2.drawContours(image, [unknown_cnt], -1, (255, 0, 0), 3)

                        #---1. get distance
                        distance_matched = self.measure_distance_between_centre_points(unknown_cx, unknown_cy,gt_cx, gt_cy,text_height,'unknown')
                        if distance_matched:

                            #---2. get intersection
                            try:
                                ji,int_area = self.calculate_intersection_area(unknown_cnt,gt_cnt,True,op=False)

                                if ji > threshold :
                                    cv2.circle(image, (unknown_cx, unknown_cy), 15, (255, 0, 0), -1)
                                    cv2.circle(image, (unknown_cx, unknown_cy), 15, (0, 255, 255), 2)
                                    # cv2.putText(image, str(ji), (unknown_cx - 20, unknown_cy - 30), cv2.FONT_HERSHEY_COMPLEX, 1,(0, 0, 255), 3, cv2.CV_AA)
                                    # --some doors/windows might be detected by system twice, so we check if already matched with gt to reduce repeated count
                                    if gt_list[gt_row_num][1] == 'M':
                                        repeat_count = repeat_count + 1
                                        unknown_list[uknown_r_num][1] = 'M'
                                    else:
                                        unknown_list[uknown_r_num][1] = 'M'
                                        gt_list[gt_row_num][1] = 'M'
                                    break
                            except:
                                print "---------------------Error occured so skipping"
                                traceback.print_exc()

        return repeat_count,image


    def measure_distance_between_centre_points(self,sys_cx, sys_cy,gt_cx,gt_cy,text_height,type):
        # -- get distance from system centre to ground truth centres
        distance = math.hypot(sys_cx - gt_cx, sys_cy - gt_cy)
        if type == 'unknown' or type =='win':
            thresh = 5
        else:
            thresh = 2.5

        if distance < text_height * thresh:
            return True

        else:
            return False

    def calculate_intersection_area(self,cnt1,cnt2,buffer,op):
        intersection_ratio, ji = 0,0
        cnt1_points = self.iterative_functions_obj.get_points_from_contour(cnt1)
        cnt2_points = self.iterative_functions_obj.get_points_from_contour(cnt2)
        if buffer:
            sys_polygon = Polygon(cnt1_points).buffer(0)
            gt_polygon = Polygon(cnt2_points).buffer(0)
        else:
            sys_polygon = Polygon(cnt1_points)
            gt_polygon = Polygon(cnt2_points)
        intersect_polygon = sys_polygon.intersection(gt_polygon)
        intersection_area = intersect_polygon.area
        # cnt1_percentage = int((intersection_area / float(cv2.contourArea(cnt1))) * 100)
        # cnt_2_percentage = int((intersection_area / float(cv2.contourArea(cnt2))) * 100)
        if op==False:
            union_area = sys_polygon.area + gt_polygon.area
            ji = intersection_area/float(union_area)
        else:
            intersection_ratio = intersection_area/float(sys_polygon.area)

        return round(ji,2), round(intersection_ratio,2)

    # def measure_intersection_area(self,system_cnt,gt_cnt):
    #     sys_points = self.iterative_functions_obj.get_points_from_contour(system_cnt)
    #     gt_points = self.iterative_functions_obj.get_points_from_contour(gt_cnt)
    #     sys_polygon = Polygon(sys_points)
    #     gt_polygon = Polygon(gt_points)
    #     intersect_polygon = sys_polygon.intersection(gt_polygon)
    #     return intersect_polygon.area

    def calculate_detection(self,sys_list, gt_list, element_type,repeat_count,ignorable_sys_cont_count):
        matched_sys_count = 0
        HC_count, matched_HC_count, MC_count,matched_MC_count = 0,0,0,0
        for row in sys_list:
            confidence = row[0][1]
            if confidence > 0.6:
                HC_count = HC_count + 1
            elif confidence > 0.3:
                MC_count = MC_count + 1
            else:
                print 'unknown element is in detections', confidence

            if row[1] is 'M':
                matched_sys_count = matched_sys_count+1
                if confidence > 0.6:
                    matched_HC_count = matched_HC_count + 1
                elif confidence > 0.3:
                    matched_MC_count = matched_MC_count + 1

        matched_gt_count = 0
        for row in gt_list:
            if row[1] is 'M':
                matched_gt_count = matched_gt_count+1

        # true_positive = matched_sys_count - repeat_count
        # false_positive = len(sys_list) - matched_sys_count
        # true_negative = len(gt_list) - matched_gt_count
        all_system_detections = len(sys_list)-ignorable_sys_cont_count

        # true_positive = matched_gt_count
        true_positive = matched_sys_count
        false_positive = all_system_detections - matched_sys_count
        # false_negative = len(gt_list) - matched_gt_count
        
        partial_sys_count = 0
        if element_type=='text':
            for row in sys_list:
                if row[1] is 'P':
                    partial_sys_count = partial_sys_count+1


        return true_positive,false_positive,len(gt_list),partial_sys_count,HC_count,MC_count,matched_HC_count,matched_MC_count


    def calculate_JI(self,gt_wall_list,sys_wall_list,sys_unknown_list,output_path,input_image,height, width,element_type,unknown):
        # wall_image = ~(np.zeros((height, width, 3), np.uint8))
        # wall_image2 = ~(np.zeros((height, width, 3), np.uint8))
        sys_walls_img = ~(np.zeros((height, width, 3), np.uint8))
        gt_walls_img = ~(np.zeros((height, width, 3), np.uint8))


        for wall_row in sys_wall_list:
            wall_data = wall_row[0]
            wall_cont = self.iterative_functions_obj.convert_points_to_contour(wall_data)
            # cv2.Contours(wall_image, [wall_cont], -1, (0, 0, 0), -1)
            # cv2.Contours(wall_image2, [wall_cont], -1, (0, 0, 255), -1)
            cv2.drawContours(sys_walls_img, [wall_cont], -1, (0, 0, 0), -1)


        for wall_row in gt_wall_list:
            wall_data = wall_row[0]
            wall_cont = self.iterative_functions_obj.convert_points_to_contour(wall_data)
            # cv2.Contours(wall_image, [wall_cont], -1, (255, 255, 255), -1)
            # cv2.Contours(wall_image2, [wall_cont], -1, (0, 0, 0), -1)
            cv2.drawContours(gt_walls_img, [wall_cont], -1, (0, 0, 0), -1)



        cv2.imwrite(output_path + '_'+element_type+'_sys_walls_img.png', sys_walls_img)
        # cv2.imwrite(output_path +'_'+element_type+ '_gt_walls_img.png', gt_walls_img)

        if unknown:
            sys_walls_img, gt_walls_img = self.erase_unknown_elements(sys_walls_img,gt_walls_img,sys_unknown_list)

        #--for cvc
        # new_gt_img_inv = self.get_intersection_of_original_and_gt(input_image,gt_walls_img,height,width)

        #--for rsvg and svg
        new_gt_img_inv = cv2.bitwise_not(gt_walls_img)
        # new_gt_img_inv_dilated = cv2.bitwise_not(gt_walls_img_dilated)

        sys_walls_img_dilated, gt_walls_img_dilated_inv = self.dilate_wall_images(sys_walls_img, new_gt_img_inv)

        cv2.imwrite(output_path + '_' + element_type + '_gt_walls_img.png', cv2.bitwise_not(new_gt_img_inv))

        # gt_jaccard_index,intersection_area = self.finding_JI(new_gt_img_inv,sys_walls_img,height, width,output_path)

        #--precision data
        gt_jaccard_index, intersection_area_precision = self.finding_JI(gt_walls_img_dilated_inv, sys_walls_img, height, width, output_path)

        #--recall data
        gt_jaccard_index, intersection_area_recall = self.finding_JI(new_gt_img_inv, sys_walls_img_dilated, height, width,output_path)

        #--paper calculations
        sys_wall_area = self.count_black_pixels(output_path,sys_walls_img,color='black')
        gt_wall_area = self.count_black_pixels(output_path, gt_walls_img, color='black')

        return gt_jaccard_index,sys_wall_area,gt_wall_area,intersection_area_precision,intersection_area_recall

    def erase_unknown_elements(self,sys_walls_img,gt_walls_img,sys_unknown_list):
        for row in sys_unknown_list:
            unknown_points, matched = row
            if matched == 'NM':
                unknown_cnt = self.iterative_functions_obj.convert_points_to_contour(unknown_points)
                cv2.drawContours(sys_walls_img, [unknown_cnt], -1, (255, 255, 255), -1)
                cv2.drawContours(gt_walls_img, [unknown_cnt], -1, (255, 255, 255), -1)
        return sys_walls_img,gt_walls_img

    def get_intersection_of_original_and_gt(self,input_image,gt_walls_img,height, width):
        # original_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)
        # --inverse gt and original results to get AND results
        gt_walls_img_inv = cv2.bitwise_not(gt_walls_img)
        input_image_inv = cv2.bitwise_not(input_image)

        contour_AND_img = ~(np.zeros((height, width, 3), np.uint8))
        cv2.bitwise_and(input_image_inv, gt_walls_img_inv, contour_AND_img, mask=None)

        return contour_AND_img

    def dilate_wall_images(self,sys_walls_img,new_gt_img_inv):
        kernel = np.ones((5, 5), np.uint8)
        sys_walls_img_inv = cv2.bitwise_not(sys_walls_img)
        # gt_walls_img_inv = cv2.bitwise_not(gt_walls_img)
        sys_walls_img_dilated = cv2.dilate(sys_walls_img_inv, kernel, iterations=1)
        gt_walls_img_dilated_inv = cv2.dilate(new_gt_img_inv, kernel, iterations=1)
        sys_walls_img_dilated_inv = cv2.bitwise_not(sys_walls_img_dilated)
        # gt_walls_img_dilated_inv = cv2.bitwise_not(gt_walls_img_dilated)
        return sys_walls_img_dilated_inv, gt_walls_img_dilated_inv

    def finding_JI(self,gt_walls_img_inv,sys_img,height, width,output_path):
        # --inverse gt and sys results to get AND, OR results
        # gt_walls_img_inv = cv2.bitwise_not(gt_img)
        sys_walls_img_inv = cv2.bitwise_not(sys_img)

        contour_OR_img = ~(np.zeros((height, width, 3), np.uint8))
        cv2.bitwise_or(sys_walls_img_inv, gt_walls_img_inv, contour_OR_img, mask=None)

        contour_AND_img = ~(np.zeros((height, width, 3), np.uint8))
        cv2.bitwise_and(sys_walls_img_inv, gt_walls_img_inv, contour_AND_img, mask=None)

        intersection_area = self.count_black_pixels(output_path,contour_AND_img,color='white')
        union_area = self.count_black_pixels(output_path,contour_OR_img,color='white')


        if intersection_area=='NA' or union_area=='NA' or union_area==0:
            gt_jaccard_index = 'NA'

        else:
            # --do calculations
            gt_jaccard_index = round((intersection_area / float(union_area)), 3)
            # print 'gt_jaccard_index', gt_jaccard_index
            #
            # cv2.imwrite(output_path + 'contour_AND_img.png', contour_AND_img)
            # cv2.imwrite(output_path + 'contour_OR_img.png', contour_OR_img)
            # cv2.imwrite(output_path+'gray AND.png',gray_contour_AND_img)
            # cv2.imwrite(output_path + 'gray OR.png', gray_contour_OR_img)
        # else:
        #     gt_jaccard_index = 0
        # print intersection_area,'/',union_area, '==', gt_jaccard_index
        # print 'gt_jaccard_index', gt_jaccard_index
        return gt_jaccard_index,intersection_area

    def count_black_pixels(self,output_path,img,color):
        cv2.imwrite(output_path + '_temp.png', img)
        img_new = Image.open(output_path + '_temp.png')
        os.remove(output_path + '_temp.png')
        color_list = img_new.getcolors()
        if color_list is None:
            return 'NA'
        else:
            if color == 'white':
                white_pixel_count = 0
                for col_row in color_list:
                    if (255, 255, 255) in col_row:
                        white_pixel_count = col_row[0]
                        break
                return white_pixel_count
            else:
                black_pixel_count = 0
                for col_row in color_list:
                    if (0, 0, 0) in col_row:
                        black_pixel_count = col_row[0]
                        break
                return black_pixel_count

    def find_missing_text_labels(self,gt_text_list,sys_text_list,text_height,input_image,output_path):
        image = input_image.copy()
        # -- a pre-processing step
        for r, row in enumerate(sys_text_list):
            sys_text_list[r] = [row] + ['NM']
        for r, row in enumerate(gt_text_list):
            gt_text_list[r] = [row] + ['NM']

        #--write to image
        for sys_row_num,sys_text_row in enumerate(sys_text_list):
            sys_text_data = sys_text_row[0]
            top_left_x, top_left_y, bottom_right_x, bottom_right_y = sys_text_data[3]
            cv2.rectangle(image, (top_left_x, top_left_y), (bottom_right_x, bottom_right_y), (0, 0, 255), 7)

        for gt_row_num, gt_text_row in enumerate(gt_text_list):
            gt_text_data = gt_text_row[0]
            top_left_x, top_left_y, bottom_right_x, bottom_right_y = gt_text_data[3]
            cv2.rectangle(image, (top_left_x, top_left_y), (bottom_right_x, bottom_right_y), (0, 255, 0), 3)
        #--end writing to image

        for sys_row_num,sys_text_row in enumerate(sys_text_list):
            sys_text_data = sys_text_row[0]
            sys_text = sys_text_data[0].replace(' ','')
            sys_cx, sys_cy = sys_text_data[2]
            # top_left_x, top_left_y, bottom_right_x, bottom_right_y = sys_text_data[3]
            # cv2.rectangle(image, (top_left_x, top_left_y), (bottom_right_x, bottom_right_y), (0, 0, 255), 7)


            for gt_row_num, gt_text_row in enumerate(gt_text_list):
                gt_text_data = gt_text_row[0]
                gt_text = gt_text_data[0].replace(' ','')
                gt_cx, gt_cy = gt_text_data[2]
                # top_left_x, top_left_y, bottom_right_x, bottom_right_y = gt_text_data[3]
                # cv2.rectangle(image, (top_left_x, top_left_y), (bottom_right_x, bottom_right_y), (0, 255, 0), 3)

                # -- get distance from system centre to ground truth centres
                distance = math.hypot(sys_cx - gt_cx, sys_cy - gt_cy)


                if distance < text_height :
                    if sys_text==gt_text :
                        sys_text_list[sys_row_num][1] = 'M'
                        gt_text_list[gt_row_num][1] = 'M'
                        cv2.circle(image, (sys_cx,sys_cy), 15, (255, 0, 0), -1)
                        cv2.circle(image, (sys_cx, sys_cy), 15, (0, 255, 255), 2)
                        cv2.putText(image, sys_text.upper(), (gt_cx - 10, gt_cy - 30),
                                    cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 3, cv2.CV_AA)
                        break
                    elif sys_text in gt_text:
                        sys_text_list[sys_row_num][1] = 'P'
                        gt_text_list[gt_row_num][1] = 'P'
                        cv2.circle(image, (sys_cx, sys_cy), 15, (255, 0, 255), -1)
                        cv2.putText(image, sys_text.upper(), (gt_cx - 10, gt_cy - 30),
                                    cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 3, cv2.CV_AA)
                        break
                        

        cv2.imwrite(output_path + '_text_matched.png', image)

        return sys_text_list,gt_text_list



    #---rooms functinos
    def find_room_matches(self,sys_rooms_list,gt_rooms_list,input_image_path,output_image_path,match_room_text,iteration,sub_area):
        sys_rooms_list,gt_rooms_list = self.match_rooms(sys_rooms_list,gt_rooms_list,input_image_path,output_image_path,match_room_text,iteration,sub_area)
        room_tp = self.count_room_matches(sys_rooms_list,gt_rooms_list)
        # return room_tp, room_fp, r_gt_total
        return room_tp



    def match_rooms(self,sys_rooms_list,gt_rooms_list,input_image_path,output_image_path,match_room_text,iteration,sub_area):
        input_image = cv2.imread(input_image_path, cv2.IMREAD_COLOR)
        # sys_rooms_list = sys_json_data['sgl_rooms_list']

        if iteration==1 or sub_area:
            # -- a pre-processing step
            for r, row in enumerate(sys_rooms_list):
                sys_rooms_list[r] = [row] + ['NM']
        if iteration==1 :
            for r, row in enumerate(gt_rooms_list):
                gt_rooms_list[r] = [row] + ['NM']

        #--match each system room with gt room
        for sys_num,system_room_row in enumerate(sys_rooms_list):
            sys_room_text, sys_room_centre, sys_room_type, sys_room_points = system_room_row[0]
            sys_cnt = self.iterative_functions_obj.convert_points_to_contour(sys_room_points)
            # if sys_room_type == 'single' and sys_room_text != 'closet' and sys_room_text != 'cellar' and sys_room_text!= 'garage':
            # if sys_room_type == 'single':
            for gt_num,gt_room_row in enumerate(gt_rooms_list):
                gt_room_text, gt_room_centre, gt_room_type, gt_room_points = gt_room_row[0]
                gt_cnt = self.iterative_functions_obj.convert_points_to_contour(gt_room_points)
                cv2.drawContours(input_image, [sys_cnt], -1, (0, 0, 255), 2)
                cv2.drawContours(input_image, [gt_cnt], -1, (0, 255, 0), 2)


                gt_match = gt_room_row[1]
                if gt_match == 'NM':
                    # gt_room_text_new = self.iterative_functions_obj. match_text_to_rooms (gt_room_text)
                    sys_text = "".join(sys_room_text.split())
                    no_punct_word = (re.sub(r'[^\w\s]', '', gt_room_text))
                    gt_text = "".join(no_punct_word.split())
                    if match_room_text:
                        if sys_text == gt_text:
                            try:
                                room_intersect_ji,int_area = self.calculate_intersection_area(sys_cnt,gt_cnt,buffer=False,op=False)
                            except:
                                room_intersect_ji,int_area = self.calculate_intersection_area(sys_cnt, gt_cnt,buffer=True,op=False)

                            if room_intersect_ji > 0.1:
                                sys_rooms_list[sys_num][1] = 'M'
                                gt_rooms_list[gt_num][1] = 'M'
                                cv2.circle(input_image, (sys_room_centre[0], sys_room_centre[1]), 15, (255, 0, 0), -1)
                                # print 'matched ',sys_room_text,gt_room_text
                                break
                    else:
                        try:
                            room_intersect_ji,int_area = self.calculate_intersection_area(sys_cnt, gt_cnt, buffer=False,op=False)
                        except:
                            room_intersect_ji,int_area = self.calculate_intersection_area(sys_cnt, gt_cnt, buffer=True,op=False)

                        if room_intersect_ji > 0.1:
                            sys_rooms_list[sys_num][1] = 'M'
                            gt_rooms_list[gt_num][1] = 'M'
                            cv2.circle(input_image, (sys_room_centre[0], sys_room_centre[1]), 15, (255, 0, 0), -1)
                            # print 'matched ',sys_room_text,gt_room_text
                            break



        cv2.imwrite(output_image_path + '_sys_rooms_img'+str(iteration)+'.png', input_image)
        # sys.exit()
        return sys_rooms_list,gt_rooms_list

    def count_room_matches(self,sys_list,gt_list):
        matched_sys_count = 0
        # ignorable_sys_cont_count = 0
        for row in sys_list:
            # text = row[0][0]
            # if text=='closet' or text == 'cellar' or text == 'garage':
            # if text=='closet':
            #     ignorable_sys_cont_count = ignorable_sys_cont_count + 1
            # else:
            if row[1] is 'M':
                matched_sys_count = matched_sys_count + 1


        # matched_gt_count = 0
        # ignorable_gt_count = 0
        # for row in gt_list:
        #     # text = row[0][0]
        #     # if text == 'cellier' or text == 'garage':
        #     #     ignorable_gt_count = ignorable_gt_count +1
        #     # else:
        #     if row[1] is 'M':
        #         matched_gt_count = matched_gt_count + 1
        #
        # all_system_detections = len(sys_list) - ignorable_sys_cont_count

        true_positive = matched_sys_count
        # false_positive = all_system_detections - matched_sys_count
        # false_negative = len(gt_list) - matched_gt_count

        # return true_positive, false_positive, len(gt_list)-ignorable_gt_count
        return true_positive

    def find_op_matches(self,sys_rooms_list,gt_rooms_list,input_image_path,output_image_path,match_room_text,iteration):
        sys_rooms_list, gt_rooms_list = self.match_rooms_op(sys_rooms_list, gt_rooms_list, input_image_path,output_image_path, match_room_text, iteration)
        room_tp = self.count_room_matches(sys_rooms_list, gt_rooms_list)
        # return room_tp, room_fp, r_gt_total
        return room_tp

    def match_rooms_op(self,sys_rooms_list,gt_rooms_list,input_image_path,output_image_path,match_room_text,iteration):
        input_image = cv2.imread(input_image_path, cv2.IMREAD_COLOR)
        if iteration == 1:
            # -- a pre-processing step
            for r, row in enumerate(sys_rooms_list):
                if sys_rooms_list[r][1] != 'NM':
                    sys_rooms_list[r][1] ='NM'
            for r, row in enumerate(gt_rooms_list):
                gt_rooms_list[r] = [row] + ['NA']

        # --match each system room with gt room
        for sys_num, system_room_row in enumerate(sys_rooms_list):
            if system_room_row[1]=='NM':
                sys_room_text, sys_room_centre, sys_room_type, sys_room_points = system_room_row[0]
                sys_cnt = self.iterative_functions_obj.convert_points_to_contour(sys_room_points)
                for gt_num, gt_room_row in enumerate(gt_rooms_list):
                    gt_room_texts, gt_room_type, gt_room_points = gt_room_row[0]
                    gt_cnt = self.iterative_functions_obj.convert_points_to_contour(gt_room_points)
                    cv2.drawContours(input_image, [sys_cnt], -1, (0, 0, 255), 2)
                    cv2.drawContours(input_image, [gt_cnt], -1, (0, 255, 0), 2)

                    sys_text = "".join(sys_room_text.split())
                    gt_texts_new = []
                    for row in gt_room_texts:
                        no_punct_word = (re.sub(r'[^\w\s]','',row[0]))
                        gt_texts_new.append("".join(no_punct_word.split()))
                    # gt_text_new = self.iterative_functions_obj.match_text_to_rooms(row[0])
                    # gt_texts_new.append(gt_text_new)

                    # gt_match = gt_room_row[1]
                    # if gt_match == 'NM':
                    if match_room_text:
                        if sys_text in gt_texts_new:
                            try:
                                room_intersect_ji,intersection_area = self.calculate_intersection_area(sys_cnt, gt_cnt, buffer=False,op=True)
                            except:
                                room_intersect_ji,intersection_area = self.calculate_intersection_area(sys_cnt, gt_cnt, buffer=True,op=True)

                            if intersection_area > 0.8:
                                sys_rooms_list[sys_num][1] = 'M'
                                # gt_rooms_list[gt_num][1] = 'M'
                                cv2.circle(input_image, (sys_room_centre[0], sys_room_centre[1]), 15, (255, 0, 0), -1)
                                break
                    else:
                        try:
                            room_intersect_ji,intersection_area = self.calculate_intersection_area(sys_cnt, gt_cnt, buffer=False,op=True)
                        except:
                            room_intersect_ji,intersection_area = self.calculate_intersection_area(sys_cnt, gt_cnt, buffer=True,op=True)

                        if intersection_area > 0.9:
                            sys_rooms_list[sys_num][1] = 'M'
                            # gt_rooms_list[gt_num][1] = 'M'
                            cv2.circle(input_image, (sys_room_centre[0], sys_room_centre[1]), 15, (255, 0, 0), -1)
                            break

        cv2.imwrite(output_image_path + '_op_sys_rooms_img'+str(iteration)+'.png', input_image)

        return sys_rooms_list, gt_rooms_list




    def find_stairs_detection(self,gt_stair_list, sys_stair_list, sys_unknown_list,output_path, input_image,height, width):

        # -- a pre-processing step
        for r, row in enumerate(sys_stair_list):
            sys_stair_list[r] = [row] + ['NM']
        for r, row in enumerate(gt_stair_list):
            gt_stair_list[r] = [row] + ['NM']


        for sys_num,sys_stair in enumerate(sys_stair_list):
            sys_cnt_points = sys_stair[0][0]
            sys_cnt = self.iterative_functions_obj.convert_points_to_contour(sys_cnt_points)
            # print cv2.contourArea(sys_cnt)
            sys_walls_img = ~(np.zeros((height, width, 3), np.uint8))
            cv2.drawContours(sys_walls_img, [sys_cnt], -1, (0, 0, 255), 3)

            for gt_num, gt_stair in enumerate(gt_stair_list):
                if gt_stair[1]=='NM':
                    gt_cnt_points = gt_stair[0][0]
                    gt_cnt = self.iterative_functions_obj.convert_points_to_contour(gt_cnt_points)
                    cv2.drawContours(sys_walls_img, [gt_cnt], -1, (0, 255, 0), 3)

                    st_intersect_ji,int_area = self.calculate_intersection_area(sys_cnt, gt_cnt, buffer=True,op=False)
                    if st_intersect_ji > 0.2:
                        sys_stair_list[sys_num][1] = 'M'
                        gt_stair_list[gt_num][1] = 'M'
                        cx,cy = self.iterative_functions_obj.get_centre_of_contour(gt_cnt)
                        cv2.circle(sys_walls_img, (cx, cy), 15, (255, 0, 0), -1)

            cv2.imwrite(output_path +'_'+str(sys_num)+'_sys_stair_img.png', sys_walls_img)



        return sys_stair_list,gt_stair_list