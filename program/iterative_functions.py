import numpy as np, cv2, os, shutil
from operator import itemgetter

class iterative_functions_class:
    def __init__(self):
        pass

    def make_directory(self, path, delete_and_write):
        # --if exists delete and remake (folder resets every time)
        if delete_and_write:
            if os.path.exists(path):
                shutil.rmtree(path)
            os.mkdir(path)
        # --else create it only if doesn't exist==don't remake
        else:
            if not (os.path.exists(path)):
                os.mkdir(path)


    # --for one set of contour points return the contour
    def convert_points_to_contour(self, point_list):
        add_bracket_cont_points = []
        for cont_point in point_list:
            add_bracket_cont_points.append([cont_point])
        contour = np.asarray(add_bracket_cont_points)
        return contour

    def get_centre_of_contour(self,contour):
        M = cv2.moments(contour)
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])
        return cx, cy

    def find_contours(self,path):
        gray_image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        ret, thresh = cv2.threshold(gray_image, 0, 255, 1)
        contours, hierachy = cv2.findContours(thresh, 1, 2)
        cont_list = []
        for cnt in contours:
            cont_list.append([cnt,cv2.contourArea(cnt)])
        cont_list.sort(key=itemgetter(1))
        return cont_list

    def get_points_from_contour(self,contour):
        contour_point_list = []
        for level1 in contour:
            for level2 in level1:
                x,y = level2
                contour_point_list.append([x,y])
        return contour_point_list

    def match_text_to_rooms(self,detected_whole_word):
        text_files_path = os.getcwd()+'/match_words.txt'
        room_data_file = open(text_files_path,'r')
        new_name = ''
        #--get text file data to a list
        rooms_connection = []
        for row_num, text_line in enumerate(room_data_file):
            line_seperate = text_line.split(':')
            if len(line_seperate) > 1:
                rooms_connection.append([])
                rooms_connection[len(rooms_connection) - 1].append(line_seperate[0].strip())
                rooms_connection[len(rooms_connection) - 1].append(line_seperate[1].strip().split(','))

        # #--match detected words with rooms in text file
        # for current_row, detected_whole_word in enumerate(text_cordinates):
        #     #--get detected word without digits
        #     # detected_whole_word = tesseract_word_row[0]
        text_word = detected_whole_word.split()
        # --check if the word has two words; 2nd word is len()==1 and is a digit
        #--- if so take only first word ex: chambre 1 ;_ we need only 'chambre'
        if len(text_word)>1 and text_word[1].isdigit():
            detected_word = text_word[0]
            digit_exists = True
        else:
            detected_word = detected_whole_word
            digit_exists = False

        word_matched = False
        for room_data_row in rooms_connection:
            room_name_to_assign = room_data_row[0]
            room_names_to_check = room_data_row[1]
            for single_room_name in room_names_to_check:
                if single_room_name==detected_word.replace(' ',''):
                    #--if is in pattern 'bedroom 1': add digit to the end of word
                    if digit_exists:
                        new_name = room_name_to_assign+' '+text_word[1]
                    else:
                        new_name = room_name_to_assign
                    word_matched = True
                    break
            if word_matched:
                break

        return new_name


    def find_centre_of_line(self,line):
        p1,p2 = line
        x1,y1 = p1
        x2,y2 = p2
        center_x, center_y= (x2+x1)/2,(y1+y2)/2
        # center_x, center_y= x1+((x2-x1)/2),y1+((y1-y2)/2)
        return center_x, center_y

    def find_points_rectangle(self,x1,y1,x2,y2):
        p1 = [x1,y1]
        p2 = [x2,y1]
        p3 = [x2,y2]
        p4 = [x1,y2]
        return p1, p2, p3, p4

    def get_image_height_width(self,path):
        gray_image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        height, width = gray_image.shape
        return height, width

    def convert_list_elements_to_tuples(self,large_list):
        tuple_list = []
        for row in large_list:
            tuple_list.append()